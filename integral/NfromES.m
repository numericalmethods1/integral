% IN THE NAME OF ALLAH (GOD)
% Written by Mahdi  2021 (https://gitlab.com/Mahdiali313) or github
% find prepare N & h for special ES (khata) (simpesom)
% see more detail on (https://gitlab.com/numericalmethods1)
clear
clc
format long

%---------------------input----------------------
disp("enter a & b like [a b]")
ab = input ('');
% ab= [0 1];

b= ab(2);
a= ab(1);

disp("enter f(x). start with @(x)")
syms x
f = input ('');
% f=@(x)exp(-x^2);%1/(1+x^3);

disp("enter ES (khata)")
ES = input ('');
% ES=0.5*10^(-6);%0.2;


%---------------------calculate--------------------

% make f"  by convert from function_handle to sym then vice versa
syms x
sf=split(func2str(f),"@(x)"); % make f as str without @(x)
sf=str2sym(sf(2));% make str to sym function
d4=diff(sf,4);%deviretion of 2 for f function
d4=matlabFunction(d4);%convert to function handle

% max of |f""|
c1=abs(d4(a));
c2=abs(d4(b));
maxF4 = max([c1 c2]);

% H >= nthroot( ES*180  /  -(b-a)*f"" ,4)
part1= (b-a)*maxF4;
h= nthroot( (ES*180) / part1 ,4);

N=(b-a)/h;

%---------------------resualt----------------------
disp(['N > ',num2str(N)])
disp(['h= ',num2str(h)])
