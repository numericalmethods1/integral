% IN THE NAME OF ALLAH (GOD)
% Written by Mahdi  2021 (https://gitlab.com/Mahdiali313) or github
% simpson morakab (integral)
% see more detail on (https://gitlab.com/numericalmethods1)
clear
clc
format long

%---------------------input----------------------
disp("enter f(x)")
f = input('');
% f=@(x)log(x);

disp("do you have values of X and Y [enter 1] or just have h [enter 0]? ")
anss=input('');

if anss == 1
disp("enter X")
x = input('');
% x=[1.2 1.3 1.4 1.5 1.6];

a=x(1);
b=x(length(x));

disp("enter Y")
y = input('');
% y=[0.1823 0.2623 0.3364 0.4054 0.47];

h = x(2)-x(1);

else if anss == 0
    disp("enter h")
    h = input('');
    
    disp("enter a & b like [a b]")
    ab = input ('');
    %ab= [0 1];

    b= ab(2);
    a= ab(1);
    %make X & Y 
    x=[a];
    for i=1:(b-a)/h
        x=[x i*h];  
    end
    y=f(x);
    
    end
end

a=x(1);
b=x(length(x));

n=length(x);
if rem(n-1,2)==1 %n must be zoj(2k) .rem=remain
    error('n must be even(zoj) number')
end

%---------------------calculate--------------------
%!!!!! matrix of matlab start from 1 , not 0
Fodd=0; %f with zarib fard in formol
Feven=0;%f with zarib zoj

i=2;
while i < n
   Fodd= Fodd+y(i);
   i=i+2;
end

i=3;
while i < n
   Feven= Feven+y(i);
   i=i+2;
end

integral= (h/3) * (y(1)+ 4*Fodd + 2*Feven + y(length(x)));

% ---------------------ES---------------------

% make f"  by convert from function_handle to sym then vice versa
syms x
sf=split(func2str(f),"@(x)"); % make f as str without @(x)
sf=str2sym(sf(2));% make str to sym function
d4=diff(sf,4);%deviretion of 4 for f function
d4=matlabFunction(d4);%convert to function handle

% max of |f"|
c1=abs(d4(a));
c2=abs(d4(b));
maxF4 = max([c1 c2]);

ES=((a-b)*maxF4*h^4)/180;

%---------------------resualt----------------------
disp(['integral f(x)= ',num2str(integral)])
disp(['|ES|= ',num2str(abs(ES))])
