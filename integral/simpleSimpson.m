% IN THE NAME OF ALLAH (GOD)
% Written by Mahdi  2021 (https://gitlab.com/Mahdiali313) or github
% simpson sade (integral)
% see more detail on (https://gitlab.com/numericalmethods1)
clear
clc
format short

%---------------------input----------------------
disp("enter a & b as [a b]")
ab = input ('');
% ab= [0 3];

b= ab(2);
a= ab(1);

disp("enter f(x). start with @(x)")
y = input ('');
% y=@(x)3*x;

%---------------------calculate--------------------
ant = ( (b-a)/6 ) * ( y(a) + y(b) + 4*y((a+b)/2) );

%---------------------resualt----------------------
disp(['integral f(x)= ',num2str(ant)])