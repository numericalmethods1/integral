% IN THE NAME OF ALLAH (GOD)
% Written by Mahdi  2021 (https://gitlab.com/Mahdiali313) or github
% zozanaghee morakab (integral)
% see more detail on (https://gitlab.com/numericalmethods1)
clear
clc
format long

%---------------------input----------------------
disp("enter a & b like [a b]")
ab = input ('');
%ab= [0 1];

b= ab(2);
a= ab(1);

disp("enter f(x). start with @(x)")
syms x
f = input ('');
%f=@(x)exp(-x^2);%1/(1+x^3);

disp("enter h")
h = input ('');
%h=0.25;%0.2;

n = (b-a) /h ;
disp(['n = ',num2str(n)])

%---------------------calculate--------------------
zarib2f=0;
for i=1:n-1
    zarib2f = zarib2f + f(i*h); 
end

integral = h/2 * ( f(0)+ 2*zarib2f + f(n*h) );

% ---------------------ET---------------------

% make f" by convert from function_handle to sym then vice versa
syms x
sf=split(func2str(f),"@(x)"); % make f as str without @(x)
sf=str2sym(sf(2));% make str to sym function
d2=diff(sf,2);%deviretion of 2 for f function
d2=matlabFunction(d2);%convert to function handle

% max of |f"|
c1=abs(d2(a));
c2=abs(d2(b));
maxFzegond = max([c1 c2]);

ET=(-h^2/12)*(b-a)*maxFzegond;

%---------------------resualt----------------------
disp(['integral f(x)= ',num2str(integral)])
disp(['|ET|= ',num2str(abs(ET))])
