% IN THE NAME OF ALLAH (GOD)
% Written by Mahdi  2021 (https://gitlab.com/Mahdiali313) or github
% Moshtagh
% see more detail on (https://gitlab.com/numericalmethods1)
clear
clc
format short

%---------------------input----------------------
disp("enter X")
x = input ('');
%x=[20 25 30];

disp("enter Y")
y = input ('');
%y=[0.34202 0.42262 0.5];

disp("enter Xi , Xi must be in X")
Xi = input ('');
%Xi=25;

i=find(x==Xi);%find position of Xi in X array to calculate f' & f"

if isempty(i)
    error('you enter incorrect Xi or X')
end

h=x(2)-x(1);

%---------------------calculate----------------------
fXi=y(i);
fXiN=y(i+1);% F(x+1)
fXiP=y(i-1);% F(x-1)

fprime= (fXiN-fXiP) / (2*h) ;
fzegond=(fXiN+fXiP-2*fXi) / (h^2) ;

%---------------------resualt----------------------

disp(['F`(x)= ',num2str(fprime)])
disp(['F"(x)= ',num2str(fzegond)])
